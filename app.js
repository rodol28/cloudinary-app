const imgPreview = document.getElementById('img-preview');
const imgUploader = document.getElementById('img-uploader');
const imgUploadBar = document.getElementById('img-upload-bar');

const cloudinaryURL =  'https://api.cloudinary.com/v1_1/dfzlexoul/image/upload';
const cloudunaryUploadPreset = 'vl0ujanl';

imgUploader.addEventListener('change', async (e) => {
    const file = e.target.files[0];

    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', cloudunaryUploadPreset);

    const res = await axios.post(cloudinaryURL, formData, {
        headers: {
            'Content-Type': 'multipart/form-data'
        },
        onUploadProgress(e) {
            console.log(Math.round((e.loaded * 100) / e.total));
            const progress = e.loaded * 100 / e.total;
            imgUploadBar.value = progress;
        }
    });
    console.log(res);
    imgPreview.src = res.data.secure_url;
});